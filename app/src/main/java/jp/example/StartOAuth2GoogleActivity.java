package jp.example;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


public class StartOAuth2GoogleActivity extends Activity {

  public static final String CLIENT_ID = "22971320806-bh92n1n03meth05cfmeffivkkce8l42q.apps.googleusercontent.com";
  public static final String CLIENT_SECRET = "KPaO54sbSPEsLvl83a5wIvtx";
  public static final String SCOPE = "https://www.googleapis.com/auth/userinfo.profile";

  protected TextView tvName;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    tvName = (TextView) findViewById(R.id.tvName);
  }


  // ボタンをクリックした時に呼ばれる
  // OAuth2GoogleActivityに逝く
  public void onClickBtn(View view) {
    Log.v("onClickBtn", "OAuth2開始！");
    Intent intent = new Intent(this, OAuth2GoogleActivity.class);
    intent.putExtra(OAuth2GoogleActivity.CLIENT_ID, CLIENT_ID);
    intent.putExtra(OAuth2GoogleActivity.CLIENT_SECRET, CLIENT_SECRET);
    intent.putExtra(OAuth2GoogleActivity.SCOPE, SCOPE);
    startActivityForResult(intent, OAuth2GoogleActivity.REQCODE_OAUTH);
  }


  // OAuth2GoogleActivityから戻ってきた時に呼ばれる
  // 認証ができていたらユーザー情報を取得する
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    Log.v("onActivityResult", "OAuth2プロセスから帰還！");
    super.onActivityResult(requestCode, resultCode, data);
    boolean validData = false;
    if (data != null) {
      String token = data.getStringExtra(OAuth2GoogleActivity.ACCESS_TOKEN);
      if (token != null) {
        new TaskGetUserInfo().execute(token);
        validData = true;
      }
    }
    if (!validData) {
      Log.v("onActivityResult", "アクセストークンなし");
      tvName.setText("OAuth2プロセスが正常に行われませんでした");
    }
  }


  // ユーザー情報取得タスク
  protected class TaskGetUserInfo extends AsyncTask<String, Void, JSONObject> {


    @Override
    protected void onPreExecute() {
      Log.v("onPreExecute", "ユーザー情報取得開始");
      tvName.setText("ユーザー情報を取得しています...");
    }


    @Override
    protected JSONObject doInBackground(String... accessTokens) {
      JSONObject userInfo = null;
      String url = "https://www.googleapis.com/oauth2/v2/userinfo" // ここに投げることになってる
              + "?access_token=" + accessTokens[0]; // OAuth2プロセスでもらった
      DefaultHttpClient client = new DefaultHttpClient();
      try {
        HttpGet httpPost = new HttpGet(url);
        HttpResponse res = client.execute(httpPost);
        HttpEntity entity = res.getEntity();
        String result = EntityUtils.toString(entity);
        Log.v("doInBackground", "result=" + result);
        userInfo = new JSONObject(result);
      } catch (ClientProtocolException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (JSONException e) {
        e.printStackTrace();
      } finally {
        client.getConnectionManager().shutdown();
      }
      return userInfo;
    }


    @Override
    protected void onPostExecute(JSONObject userInfo) {
      if (userInfo == null) {
        tvName.setText("ユーザー情報取得失敗（userInfoがNULL）");
        Log.v("onPostExecute", "ユーザー情報取得失敗 userInfoがNULL");
      } else {
        String name = null;
        try {
          name = userInfo.getString("name");
          tvName.setText("あなたの名前は [" + name + "] ですね？");
          Log.v("onPostExecute", "ユーザー情報取得成功 name=" + name + " userInfo=" + userInfo.toString());
        } catch (JSONException e) {
          tvName.setText("ユーザー情報取得失敗");
          Log.v("onPostExecute", "ユーザー情報取得失敗 userInfo=" + userInfo.toString());
          e.printStackTrace();
        }
      }
    }


  } // END class TaskGetUserInfo


} // END class StartOAuth2GoogleActivity