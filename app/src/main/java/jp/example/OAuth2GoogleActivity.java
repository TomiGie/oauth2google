package jp.example;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;


public class OAuth2GoogleActivity extends Activity {


  public static final int REQCODE_OAUTH = 0;
  public static final String CLIENT_ID = "client_id";
  public static final String CLIENT_SECRET = "client_secret";
  public static final String SCOPE = "scope";
  public static final String ACCESS_TOKEN = "access_token";

  protected String clientId;
  protected String clientSecret;
  protected String scope;

  protected ViewSwitcher vs;
  protected TextView tvProg;
  protected WebView wv;


  @Override
  public void onCreate(Bundle savedInstanceState) {

    // お約束
    super.onCreate(savedInstanceState);
    setContentView(R.layout.oauth2google);

    // Intentからパラメータ取得
    Intent intent = getIntent();
    clientId = intent.getStringExtra(CLIENT_ID);
    clientSecret = intent.getStringExtra(CLIENT_SECRET);
    scope = intent.getStringExtra(SCOPE);

    // 各種Viewを取得
    vs = (ViewSwitcher) findViewById(R.id.vs);
    tvProg = (TextView) findViewById(R.id.tvProg);
    wv = (WebView) findViewById(R.id.wv);
      WebSettings settings = wv.getSettings();
      settings.setJavaScriptEnabled(true);

    // WebView設定
    wv.setWebViewClient(new WebViewClient() { // これをしないとアドレスバーなどが出る


      @Override
      public void onPageFinished(WebView view, String url) { // ページ読み込み完了時

        // ページタイトルからコードを取得
        String title = view.getTitle();
        String code = getCode(title);

        // コード取得成功ページ以外
        if (code == null) {
          Log.v("onPageFinished", "コード取得成功ページ以外 url=" + url);
          if (!(vs.getCurrentView() instanceof WebView)) { // WebViewが表示されてなかったら
            vs.showNext(); // Web認証画面表示
          }
        }

        // コード取得成功
        else {
          Log.v("onPageFinished", "コード取得成功 code=" + code);
          vs.showPrevious(); // プログレス画面に戻る
          new TaskGetAccessToken().execute(code); // アクセストークン取得開始
        }

      }
    });

    // 認証ページURL
    String url = "https://accounts.google.com/o/oauth2/auth" // ここに投げることになってる
            + "?client_id=" + clientId // アプリケーション登録してもらった
            + "&response_type=code" // InstalledAppだとこの値で固定
            + "&redirect_uri=urn:ietf:wg:oauth:2.0:oob" // タイトルにcodeを表示する場合は固定
            + "&scope=" + URLEncoder.encode(scope); // 許可を得たいサービス

    Log.v("onCreate", "clientId=" + clientId + " clientSecret=" + clientSecret + " scope=" + scope + " url=" + url);

    // 認証ページロード開始
    wv.loadUrl(url);

  }


  /**
   * 認証成功ページのタイトルは「Success code=XXXXXXX」という風になっているので、
   * このタイトルから「code=」以下の部分を切り出してOAuth2アクセスコードとして返す
   *
   * @param title
   *          ページタイトル
   * @return OAuth2アクセスコード
   */
  protected String getCode(String title) {
    String code = null;
    String codeKey = "code=";
    int idx = title.indexOf(codeKey);
    if (idx != -1) { // 認証成功ページだった
      code = title.substring(idx + codeKey.length()); // 「code」を切り出し
    }
    return code;
  }


  // アクセストークン取得タスク
  protected class TaskGetAccessToken extends AsyncTask<String, Void, String> {


    @Override
    protected void onPreExecute() {
      Log.v("onPostExecute", "アクセストークン取得開始");
      tvProg.setText("アクセストークンを取得中...");
    }


    @Override
    protected String doInBackground(String... codes) {
      String token = null;
      DefaultHttpClient client = new DefaultHttpClient();
      try {

        // パラメータ構築
        ArrayList<NameValuePair> formParams = new ArrayList<NameValuePair>();
        formParams.add(new BasicNameValuePair("code", codes[0]));
        formParams.add(new BasicNameValuePair("client_id", clientId));
        formParams.add(new BasicNameValuePair("client_secret", clientSecret));
        formParams.add(new BasicNameValuePair("redirect_uri", "urn:ietf:wg:oauth:2.0:oob"));
        formParams.add(new BasicNameValuePair("grant_type", "authorization_code"));

        // トークンの取得はPOSTで行うことになっている
        HttpPost httpPost = new HttpPost("https://accounts.google.com/o/oauth2/token");
        httpPost.setEntity(new UrlEncodedFormEntity(formParams, "UTF-8")); // パラメータセット
        HttpResponse res = client.execute(httpPost);
        HttpEntity entity = res.getEntity();
        String result = EntityUtils.toString(entity);

        // JSONObject取得
        JSONObject json = new JSONObject(result);
        if (json.has("access_token")) {
          token = json.getString("access_token");
        } else {
          if (json.has("error")) {
            String error = json.getString("error");
            Log.d("getAccessToken", error);
          }
        }

      } catch (ClientProtocolException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (JSONException e) {
        e.printStackTrace();
      } finally {
        client.getConnectionManager().shutdown();
      }
      return token;
    }


    @Override
    protected void onPostExecute(String token) {
      if (token == null) {
        Log.v("onPostExecute", "アクセストークン取得失敗");
      } else {
        Log.v("onPostExecute", "アクセストークン取得成功 token=" + token);
//        Intent intent = new Intent();
//        intent.putExtra(ACCESS_TOKEN, token);
//        setResult(Activity.RESULT_OK, intent);
          reequestApi(token);
      }
//      finish();
    }

  } // END class TaskGetAccessToken


    private OAuthConsumer mConsumer;
    private OAuthProvider mProvider;

    public void reequestApi(String token) {
        mConsumer = new CommonsHttpOAuthConsumer("22971320806-bh92n1n03meth05cfmeffivkkce8l42q.apps.googleusercontent.com", "KPaO54sbSPEsLvl83a5wIvtx");
        HttpClient httpClient = new DefaultHttpClient();
        String urlString = "https://betaspike.appspot.com";

        // 非同期タスクを定義
        HttpPostTask task = new HttpPostTask(
                this,
                urlString, token,

                // タスク完了時に呼ばれるUIのハンドラ
                new HttpPostHandler(){

                    @Override
                    public void onPostCompleted(String response) {
                        // 受信結果をUIに表示
                    }

                    @Override
                    public void onPostFailed(String response) {
                        Toast.makeText(
                                getApplicationContext(),
                                "エラーが発生しました。",
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
        );
        task.addPostParam( "params", "d71de0bc6b0345289118dbce675f850d.16" );

        // タスクを開始
        task.execute();
    }
    public class HttpPostTask extends AsyncTask<Void, Void, Void> {

        // 設定事項
        private String request_encoding = "UTF-8";
        private String response_encoding = "UTF-8";
        private String token;

        // 初期化事項
        private Activity parent_activity = null;
        private String post_url = null;
        private Handler ui_handler = null;
        private List<NameValuePair> post_params = null;

        // 処理中に使うメンバ
        private ResponseHandler<Void> response_handler = null;
        private String http_err_msg = null;
        private String http_ret_msg = null;
        private ProgressDialog dialog = null;


        // 生成時
        public HttpPostTask( Activity parent_activity, String post_url, String token, Handler ui_handler )
        {
            // 初期化
            this.parent_activity = parent_activity;
            this.post_url = post_url;
            this.ui_handler = ui_handler;
            this.token = token;

            // 送信パラメータは初期化せず，new後にsetさせる
            post_params = new ArrayList<NameValuePair>();
        }


  /* --------------------- POSTパラメータ --------------------- */


        // 追加
        public void addPostParam( String post_name, String post_value )
        {
            post_params.add(new BasicNameValuePair( post_name, post_value ));
        }


  /* --------------------- 処理本体 --------------------- */


        // タスク開始時
        protected void onPreExecute() {
            // ダイアログを表示
            dialog = new ProgressDialog( parent_activity );
            dialog.setMessage("通信中・・・");
            dialog.show();

            // レスポンスハンドラを生成
            response_handler = new ResponseHandler<Void>() {

                // HTTPレスポンスから，受信文字列をエンコードして文字列として返す
                @Override
                public Void handleResponse(HttpResponse response) throws IOException
                {
                    Log.d(
                            "posttest",
                            "レスポンスコード：" + response.getStatusLine().getStatusCode()
                    );

                    // 正常に受信できた場合は200
                    switch (response.getStatusLine().getStatusCode()) {
                        case HttpStatus.SC_OK:
                            Log.d("posttest", "レスポンス取得に成功");

                            // レスポンスデータをエンコード済みの文字列として取得する。
                            // ※IOExceptionの可能性あり
                            HttpPostTask.this.http_ret_msg = EntityUtils.toString(
                                    response.getEntity(),
                                    HttpPostTask.this.response_encoding
                            );
                            Log.d("", "" + response.toString());
                            break;

                        case HttpStatus.SC_NOT_FOUND:
                            // 404
                            Log.d("posttest", "存在しない");
                            HttpPostTask.this.http_err_msg = "404 Not Found";
                            break;

                        default:
                            Log.d("posttest", "通信エラー");
                            HttpPostTask.this.http_err_msg = "通信エラーが発生";
                    }

                    return null;
                }

            };
        }


        // メイン処理
        protected Void doInBackground(Void... unused) {

            Log.d("posttest", "postします");

            // URL
            URI url = null;
            try {
                url = new URI( post_url );
                Log.d("posttest", "URLはOK");
            } catch (URISyntaxException e) {
                e.printStackTrace();
                http_err_msg = "不正なURL";
                return null;
            }

            // POSTパラメータ付きでPOSTリクエストを構築
            HttpPost request = new HttpPost( url );
            try {
                // 送信パラメータのエンコードを指定
                request.addHeader("Authorization", token);
                request.setEntity(new UrlEncodedFormEntity(post_params, request_encoding));
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
                http_err_msg = "不正な文字コード";
                return null;
            }

            // POSTリクエストを実行
            DefaultHttpClient httpClient = new DefaultHttpClient();
            Log.d("posttest", "POST開始");
            try {
                httpClient.execute(request, response_handler);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                http_err_msg = "プロトコルのエラー";
            } catch (IOException e) {
                e.printStackTrace();
                http_err_msg = "IOエラー";
            }

            // shutdownすると通信できなくなる
            httpClient.getConnectionManager().shutdown();

            return null;
        }


        // タスク終了時
        protected void onPostExecute(Void unused) {
            // ダイアログを消す
            dialog.dismiss();

            // 受信結果をUIに渡すためにまとめる
            Message message = new Message();
            Bundle bundle = new Bundle();
            if (http_err_msg != null) {
                // エラー発生時
                bundle.putBoolean("http_post_success", false);
                bundle.putString("http_response", http_err_msg);
            } else {
                // 通信成功時
                bundle.putBoolean("http_post_success", true);
                bundle.putString("http_response", http_ret_msg);
            }
            message.setData(bundle);

            // 受信結果に基づいてUI操作させる
            ui_handler.sendMessage(message);
        }

    }
    public abstract class HttpPostHandler extends Handler {

        // このメソッドは隠ぺいし，Messageなどの低レベルオブジェクトを
        // 直接扱わないでもよいようにさせる
        public void handleMessage(Message msg)
        {
            boolean isPostSuccess = msg.getData().getBoolean("http_post_success");
            String http_response = msg.getData().get("http_response").toString();

            if( isPostSuccess )
            {
                onPostCompleted( http_response );
            }
            else
            {
                onPostFailed( http_response );
            }
        }


        // 下記をoverrideさせずに抽象化した理由は，本クラス指定時に
        // 「実装されていないメソッドの追加」でメソッドスタブを楽に自動生成させるため。
        // また，異常系の処理フローも真剣にコーディングさせるため。


        // 通信成功時の処理を記述させる。
        // 名前をonPostSuccessではなくonPostCompletedにした理由は，
        // メソッド自動生成時に正常系が先頭に来るようにするため。
        public abstract void onPostCompleted( String response );

        // 通信失敗時の処理を記述させる
        public abstract void onPostFailed( String response );

    }

} // END class OAuth2GoogleActivity
